<?
$MESS["tiptoppay.kassir_MODULE_NAME"] = "Онлайн-Касса TipTopPayKassir (ФЗ-54)";
$MESS["tiptoppay.kassir_MODULE_DESC"] = "Модуль позволяет после оплаты отправлять данные на онлайн-кассы сервиса TipTopPay";
$MESS["tiptoppay.kassir_PARTNER_NAME"] = "TipTopPay";
$MESS["tiptoppay.kassir_PARTNER_URI"] = "https://tiptoppay.kz/";