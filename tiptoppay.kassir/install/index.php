<?

IncludeModuleLangFile(__FILE__);
if (class_exists('tiptoppay_kassir')) return;
Class tiptoppay_kassir extends CModule
{
    const MODULE_ID = 'tiptoppay.kassir';
    var $MODULE_ID = 'tiptoppay.kassir';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $strError = '';

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__)."/version.php");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = GetMessage("tiptoppay.kassir_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("tiptoppay.kassir_MODULE_DESC");
        $this->PARTNER_NAME = GetMessage("tiptoppay.kassir_PARTNER_NAME");
        $this->PARTNER_URI = GetMessage("tiptoppay.kassir_PARTNER_URI");
    }

    function InstallDB($arParams = array())
    {
        global $DB, $DBType, $APPLICATION;
        $this->errors = false;
        $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/db/mysql/install.sql");
        if($this->errors !== false)
        {
            $APPLICATION->ThrowException(implode("", $this->errors));
            return false;
        }
        return true;

    }

    function UnInstallDB($arParams = array())
    {
        global $DB, $DBType, $APPLICATION;
        $this->errors = false;
        $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/db/mysql/uninstall.sql");
        if($this->errors !== false)
        {
            $APPLICATION->ThrowException(implode("", $this->errors));
            return false;
        }
        return true;

    }

    function InstallEvents()
    {
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler("sale", "OnSaleOrderPaid", $this->MODULE_ID, "CTipTopPaykassir", "OnSaleOrderPaid",9999);
        $eventManager->registerEventHandler("main", "OnAdminContextMenuShow", $this->MODULE_ID, "CTipTopPaykassir", "OnAdminContextMenuShowHandler",9999);
		    $eventManager->registerEventHandler("sale", "OnSaleStatusOrder", $this->MODULE_ID, "CTipTopPaykassir", "OnTipTopPayKassirStatusUpdate",9999);


        return true;
    }

    function UnInstallEvents()
    {
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->unRegisterEventHandler("sale", "OnSaleOrderPaid", $this->MODULE_ID, "CTipTopPaykassir", "OnSaleOrderPaid",9999);
        $eventManager->unRegisterEventHandler("main", "OnAdminContextMenuShow", $this->MODULE_ID, "CTipTopPaykassir", "OnAdminContextMenuShowHandler",9999);
		$eventManager->unRegisterEventHandler("sale", "OnSaleStatusOrder", $this->MODULE_ID, "CTipTopPaykassir", "OnTipTopPayKassirStatusUpdate",9999);
        return true;
    }

    function InstallFiles($arParams = array())
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/tools", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools",true,true);
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx('/bitrix/tools/'.$this->MODULE_ID."/");
       return true;
    }

    function DoInstall()
    {
        global $APPLICATION;
        RegisterModule(self::MODULE_ID);
        $this->InstallFiles();
        $this->InstallDB();
        $this->InstallEvents();
    }

    function DoUninstall()
    {
        global $APPLICATION;
        $this->UnInstallEvents();
        $this->UnInstallDB();
        $this->UnInstallFiles();
        UnRegisterModule(self::MODULE_ID);
    }


}
?>